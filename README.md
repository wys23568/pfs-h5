- Vue 2.x
- Vuex
- Vue Router
- ES6
- webpack
- axios
- Node.js

### 本地开发运行
- 在项目根文件夹下先后执行命令 `npm install` 、 `npm run dev`
- 商城前台端口默认9999 http://localhost:9999
## 部署
- 先后执行命令 `npm install` 、 `npm run build` 将打包生成的 `dist` 静态文件放置服务器中，若使用Nginx等涉及跨域请配置路由代理

## num/200px

> page 目录
  >> Home       首页   (√) 
  >> Login      登录   (√) 
  >> carList    车型列表页   (√) 
  >> PfsProduct 金融产品介绍   (√) 
  >> PfsCalculator 金融产品计算器   (√) 
  >> Center     个人中心   (√) 
  >> Dealer     查找经销商   (√) 
  >> leaveInfo  在线留资   (√) 
  >> History    历史留资记录
  >> HistoryAudit    历史预审核记录
  >> About    关于我们
  >> TestDrive    预约试驾
  