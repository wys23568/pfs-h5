import Vue from 'vue'
import Router from 'vue-router'
const Home = () => import('/page/Home/home.vue')
const carList = () => import('/page/carList/carList.vue')
const PfsProduct = () => import('/page/PfsProduct/PfsProduct.vue')
const PfsCalculator = () => import('/page/PfsCalculator/PfsCalculator.vue')
const idcard = () => import('/page/PfsCalculator/applyInfo/idcard.vue')
const bankcard = () => import('/page/PfsCalculator/applyInfo/bankCard.vue')
const info = () => import('/page/PfsCalculator/applyInfo/info.vue')
const aresult = () => import('/page/PfsCalculator/applyInfo/result.vue')

const Center = () => import('/page/Center/Center.vue')
const editName = () => import('/page/Center/editName/editName.vue')
const editCar = () => import('/page/Center/editCar/editCar.vue')
const editCarModel = () => import('/page/Center/editCar/editCarModel.vue')

const HistoryAudit = () => import('/page/HistoryAudit/HistoryAudit.vue')
const History = () => import('/page/History/History.vue')
const HistoryDetail = () => import('/page/History/detail/detail.vue')
const Dealer = () => import('/page/Dealer/Dealer.vue')
const About = () => import('/page/About/About.vue')
const LeaveInfo = () => import('/page/leaveInfo/leaveInfo.vue')
const TestDrive = () => import('/page/TestDrive/TestDrive.vue')

const Login = () => import('/page/Login/login.vue')
const Privacy = () => import('/page/Privacy/Privacy.vue')

Vue.use(Router)
export default new Router({
  routes: [
    {path: '/',component: Home,name: 'home',},
    {path: '/carList', name: 'carList', component: carList},
    {path: '/PfsProduct', name: 'PfsProduct', component: PfsProduct,},
    {path: '/PfsCalculator', name: 'PfsCalculator', component: PfsCalculator,},
    {path: '/idcard', name: 'idcard', component: idcard, meta: {keepAlive: true,}},
    {path: '/bankcard', name: 'bankcard', component: bankcard,meta: {keepAlive: true,}},
    {path: '/info', name: 'info', component: info,meta: {keepAlive: true,}},
    {path: '/aresult', name: 'aresult', component: aresult,},
    
    {path: '/Center', name: 'Center', component: Center,},
    {path: '/editName', name: 'editName', component: editName,},
    {path: '/editCar', name: 'editCar', component: editCar,},
    {path: '/editCarModel', name: 'editCarModel', component: editCarModel,},
    
    {path: '/HistoryAudit', name: 'HistoryAudit', component: HistoryAudit,},
    {path: '/History', name: 'History', component: History,},
    {path: '/HistoryDetail', name: 'HistoryDetail', component: HistoryDetail,},
    {path: '/Dealer', name: 'Dealer', component: Dealer,meta: {keepAlive: true,}},
    {path: '/About', name: 'About', component: About,},
    {path: '/LeaveInfo', name: 'LeaveInfo', component: LeaveInfo,meta: {keepAlive: true,}},
    {path: '/TestDrive', name: 'TestDrive', component: TestDrive,meta: {keepAlive: true,}},

    
    {path: '/login', name: 'login', component: Login,meta: {keepAlive: true,}},
    {path: '/privacy', name: 'Privacy', component: Privacy},
    
    {path: '*', redirect: '/home'}
  ]
})
