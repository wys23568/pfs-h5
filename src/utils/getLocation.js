let getLocation = function () {
  return new Promise(function (resolve, reject) {
    var geolocation = new BMap.Geolocation(); 
    geolocation.getCurrentPosition(function(r){
      if(this.getStatus() == BMAP_STATUS_SUCCESS){ 
        var myGeo = new BMap.Geocoder();
        myGeo.getLocation(new BMap.Point(r.point.lng, r.point.lat), function(result){
          resolve(result)
        });
      }
    });
  })
}

// if(navigator.geolocation){  
//   navigator.geolocation.getCurrentPosition(
//     function (p) {
//       alert(p.coords.latitude)
//     },
//     function (e) {
//       switch (e.code) {  
//           case e.TIMEOUT:  
//               alert("定位失败，请求获取用户位置超时");  
//               break;  
//           case e.PERMISSION_DENIED:  
//               alert("您拒绝了使用位置服务功能，查询已取消");  
//               break;  
//           case e.POSITION_UNAVAILABLE:  
//               alert("抱歉，暂时无法为您所在的星球提供位置服务");  
//               break;  
//           case e.UNKNOWN_ERROR:  
//               alert("发生一个位置错误");  
//               break;  
//       }  
//     }
//   )
// }

export default getLocation