import Vue from 'vue'
import Vuex from 'vuex'
import action from './modules/action'
import getters from './getters'
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    action,
  },
  getters,
})
