import VueCookie from 'vue-cookie'
const app = {
  state: {
    language: VueCookie.get('language') || 'zh',
  },
  mutations: {
    // 中英文
    SET_LANGUAGE: (state, language) => {
      state.language = language
      VueCookie.set('language', language)
    },
  },
  actions: {
    // 中英文
    setLanguage({ commit }, language) {
      commit('SET_LANGUAGE', language)
    },
  }
}

export default app
