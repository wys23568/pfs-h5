import Vue from 'vue';
import VueCookie from 'vue-cookie'
import axios from 'axios';
import { getStore,setStore } from '/utils/storage.js'
import { Toast,Indicator } from 'mint-ui';
// console.log(process.env.NODE_ENV)
if(process.env.NODE_ENV == "development") {
  axios.defaults.baseURL = 'https://dev-pfs-portal-api.mhpchina.com.cn';
} else if (process.env.NODE_ENV == "production") {
  axios.defaults.baseURL = 'https://qa-pfs-portal-api.mhpchina.com.cn';
  // axios.defaults.baseURL = 'https://dev-pfs-portal-api.mhpchina.com.cn';
}

axios.defaults.timeout = 10000;
axios.defaults.headers.post['Content-Type'] = 'application/x-www=form-urlencoded';

axios.interceptors.request.use(config => {
  config.headers.Authorization = getStore("userInfo")!=null && getStore("userInfo") != "" ? 'Bearer '+JSON.parse(getStore("userInfo")).token : "";
  Indicator.open({
    spinnerType: 'fading-circle'
  });
  return config
}, error => {
  return Promise.reject(error)
})
// http响应拦截器
axios.interceptors.response.use(data => {// 响应成功关闭loading
  Indicator.close();
  return data
}, error => {
  Indicator.close();
  return Promise.reject(error)
})
export default {
  fetchGet(url, params = {}) {
    return new Promise((resolve, reject) => {
      axios.get(url, { params }).then(res => {
        if (res.data.code == 200) {
          resolve(res.data)
        } else if (res.data.code == 401) {
          VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(res.data.message) : Toast(res.data.messageEn);
          // Toast(res.data.message);
          window.location.hash = '#/login';
          setStore("hasLogin", false);
        } else {
          VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(res.data.message) : Toast(res.data.messageEn);
          // Toast(res.data.message);
        }
      }).catch(error => {
        if(error.response.status == 401) {
          window.location.hash = '#/login';
          setStore("hasLogin", false);
        }
        console.log(VueCookie.get('language'))
        VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(error.response.data.message) : Toast(error.response.data.messageEn);
        // Toast(error.response.data.message);
        reject(error);
      })
    })
  },
  fetchPost(url, params = {}) {
    return new Promise((resolve, reject) => {
      axios.post(url, params).then(res => {
        if (res.data.code == 200) {
          resolve(res.data)
        } else if (res.data.code == 401) {
          VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(res.data.message) : Toast(res.data.messageEn);
          // Toast(res.data.message);
          window.location.hash = '#/login';
          setStore("hasLogin", false);
        } else {
          resolve(res.data)
          VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(res.data.message) : Toast(res.data.messageEn);
          // Toast(res.data.message);
        }
      }).catch(error => {
        if(error.response.status == 401) {
          window.location.hash = '#/login';
          setStore("hasLogin", false);
        }
        VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(error.response.data.message) : Toast(error.response.data.messageEn);
        // Toast(error.response.data.message);
        reject(error);
      })
    })
  },
  fetchPut(url, params = {}) {
    return new Promise((resolve, reject) => {
      axios.put(url, params).then(res => {
        if (res.data.code == 200) {
          resolve(res.data)
        } else if (res.data.code == 401) {
          VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(res.data.message) : Toast(res.data.messageEn);
          // Toast(res.data.message);
          window.location.hash = '#/login';
          setStore("hasLogin", false);
        } else {
          resolve(res.data)
          VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(res.data.message) : Toast(res.data.messageEn);
          // Toast(res.data.message);
        }
      }).catch(error => {
        if(error.response.status == 401) {
          window.location.hash = '#/login';
          setStore("hasLogin", false);
        }
        VueCookie.get('language') == null || VueCookie.get('language') == "zh" ? Toast(error.response.data.message) : Toast(error.response.data.messageEn);
        // Toast(error.response.data.message);
        reject(error);
      })
    })
  }
}
