import http from './public'
// 新版接口
// 首页接口
export const homeApi = (params) => {
  return http.fetchGet('/sms/home/1', params)
}
// 车型列表接口(分页获取车型基本信息)
export const carlistApi = (params) => {
  return http.fetchGet('/carmodel/page', params)
}
// 金融服务-车型列表头部filter数据来源
export const carlistFilter = (params) => {
  return http.fetchGet('/carmodel/filter/condition', params)
}
// 根据车型查询可用的金融产品
export const pfsProducts = (params) => {
  return http.fetchGet(`/product/carmodel/${params}/products`)
}
// 金融服务-车型产品详情
export const pfsCalculator = (params) => {
  return http.fetchGet(`/product/carmodel/${params.id}`,params)
}
// 金融计算器-保时捷金融,保时捷金融 S,保时捷金融 50% - 50%
export const calculator = (params) => {
  return http.fetchPost('/product/pmt/calculator', params)
}
// 金融计算器-阶段年供
export const calculator_py = (params) => {
  return http.fetchPost('/product/py/calculator', params)
}
// 金融计算器-特定月供
export const calculator_pv = (params) => {
  return http.fetchPost('/product/pv/calculator', params)
}

// 发送短信验证码
export const smscode = (params) => {
  // return http.fetchGet(`/sso/smscode/${params.mobile}`,params)
  return http.fetchGet(`/umsmember/smscode/${params.mobile}`,params)
}
// 手机号码+短信验证码登录
export const login = (params) => {
  return http.fetchPost('/umsmember/login',params)
}

// 根据经纬度查询附近指定范围内的经销商
export const dealerDistance = (params) => {
  return http.fetchGet(`/dealer/distance`,params)
}
// 根据定位经纬度获取城市信息(经度在前，纬度在后，经纬度间以“,”分割，经纬度小数点后不要超过 6 位)
export const dealerLocation = (params) => {
  console.log(params)
  return http.fetchGet(`/area/location/${params.location}`)
}
// 获取所有省市信息
export const getAllProvinceCity = () => {
  return http.fetchGet(`/area/allProvinceCity`)
}

// 所有车系
export const getCarseries = () => {
  return http.fetchGet(`/carseries`)
}
// 根据车系获取车型
export const getCarModel = (params) => {
  return http.fetchGet(`/carmodel/series/${params.seriesId}`,params)
}
//期望月供
export const getMonthCost = (params) => {
  return http.fetchGet(`/dict/type/${params.typeName}`,params)
}
//在线留资 在线留资提交
export const submitLead = (params) => {
  return http.fetchPost(`/lead`,params)
}

// 历史留资记录列表
export const getLeadPageList = (params) => {
  return http.fetchGet(`/lead/page`,params)
}
// 历史留资记录详情
export const getLeadDetail = (params) => {
  return http.fetchGet(`/lead/${params.leadId}`,params)
}

// 获取个人信息
export const getUserInfo = (params) => {
  return http.fetchGet(`/umsmember`)
}
// 更新个人信息
export const updateUserInfo = (params) => {
  return http.fetchPut(`/umsmember`,params)
}
// 用户注销登录
export const loginOut = (params) => {
  return http.fetchGet(`/umsmember/loginOut`,params)
}

// 用户注销登录
export const share = (params) => {
  return http.fetchGet(`/wx/wxconfig`,params)
}
// 身份证OCR识别-双面识别
export const idCardAllApi = (params) => {
  return http.fetchPost(`/ocr/idCard`,params)
}
// 银行卡OCR识别
export const bankCardApi = (params) => {
  return http.fetchPost(`/ocr/bankCard`,params)
}

// 银行卡预留手机号 验证码
export const bankSmsCode = (params) => {
  return http.fetchGet(`/preAudit/bankSmsCode`,params)
}
// 银行卡OCR识别
export const validateBankSmsCode = (params) => {
  return http.fetchPost(`/preAudit/validateBankSmsCode?mobile=${params.mobile}&smsCode=${params.smsCode}`,params)
}
