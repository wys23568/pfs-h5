import Vue from 'vue'
import VueI18n from 'vue-i18n'
import VueCookie from 'vue-cookie'
import enLocale from './en'
import zhLocale from './zh'

Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
  },
  zh: {
    ...zhLocale,
  }
}

const i18n = new VueI18n({
  locale: VueCookie.get('language') || 'zh', // set locale
  // locale: 'zh', // set locale
  messages // set locale messages
})
export default i18n
