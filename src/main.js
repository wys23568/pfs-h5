import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/'
import VueLazyload from 'vue-lazyload'
import infiniteScroll from 'vue-infinite-scroll'
import VueCookie from 'vue-cookie'
import i18n from './common/lang'
import api from './api/public'
// import { userInfo } from './api'
import { Swipe, SwipeItem, Field, Search, Header, InfiniteScroll,Loadmore, Spinner,Range,Button,Radio,Actionsheet, } from 'mint-ui'


import VueContentPlaceholders from 'vue-content-placeholders'


Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Field.name, Field);
Vue.component(Search.name, Search);
Vue.component(Header.name, Header);
Vue.use(InfiniteScroll);
Vue.component(Loadmore.name, Loadmore);
Vue.component(Spinner.name, Spinner);
Vue.component(Range.name, Range);
Vue.component(Button.name, Button);
Vue.component(Radio.name, Radio);
Vue.component(Actionsheet.name, Actionsheet);
Vue.use(infiniteScroll)
Vue.use(VueCookie)
Vue.use(VueLazyload, {
  loading: '/static/images/load.gif'
})
Vue.config.productionTip = false;
Vue.filter('NumFormat', function(value) {
  var intPart =  Number(value)|0; 
  var intPartFormat = intPart.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
  return intPartFormat;
})
Vue.filter('Number',function(value) {
  return Number(value);
})
Vue.filter('Fixed',function(value) {
  var number = ''+value;
  var intNumber = number.substring(0,number.indexOf(".")+3);
  return intNumber
})
new Vue({
  el: '#app',
  i18n,
  store,
  router,
  render: h => h(App)
})
